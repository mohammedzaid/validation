<!doctype html>

<html class="no-js" lang="">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
		
		<!-- Font -->
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<!-- Font -->
		
		
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
	
	    <script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		 ga('create', 'UA-56895014-4', 'auto');
		 ga('send', 'pageview');

	    </script>	
    </head>
    <body>
    <header id="home">
            
            <!-- Main Menu Start -->
            <div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a href="http://emr.smartideasinc.in/" class="navbar-brand"><img src="img/logo.png" alt="Logo" /></a>                           
                            </div>
                            
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="aboutus.php">About</a></li>
                                    <li><a href="indexlog.php">Sign in</a></li>
                                    
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main Menu End -->
            
            <!-- Sider Start -->
            <div class="slider">
                <div id="fawesome-carousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators indicatior2">
                        <li data-target="#fawesome-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#fawesome-carousel" data-slide-to="1"></li>
                    </ol>
                 
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img style="width: 1600px ;height: 480px" src="img/slider1.jpg" alt="Sider Big Image">
                            <div class="carousel-caption">
                                <h1 class="wow fadeInLeft">The #1 cloud-based electronic Medical record platform for doctors and patients</h1>
                                <div class="slider-btn wow fadeIn">
                                    <a href="demo_signup.php" class="btn btn-learn">Sign up for FREE !</a>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <img style="width: 1600px ;height: 480px" src="img/slider2.jpg" alt="Sider Big Image">
                            <div class="carousel-caption">
                                <h1 class="wow fadeInLeft">Deliver better care more efficiently withthe #1 electronic Medical record (EMR)</h1>
                                <div class="slider-btn wow fadeIn">
                                    <a href="demo_signup.php" class="btn btn-learn">Sign up for FREE !</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Sider End -->
            
        </header>
        <br>
        <div class="container">
            <div class="col-sm-12">
                        <div class="title">
                            <h3><span>WEB BASED ELECTRONIC MEDICAL REPORT SOFTWARE</span></h3>
                        </div>
                        
                        <p align="justify" style="font-size:15px">EMR introduces a better patient experience through the cloud With no software to download or hardware to manage.
                                Smart Ideas Inc can help you with the service of EMR (Electronic Medical Record) by giving you the application which is more accurate and complete clinical information about a patient.
                                An Electronic Medical Record (EMR) is a secured digital version of a paper based record for a patient. Within a single healthcare facility, EMR goes beyond just data and contains comprehensive medical and treatment records like error free patient history, customized and scalable reports, medical transcription, billing information and other important details for a complete patient profile.
                                This application is available anytime , anywhere just through a touch. This helps a common man by updating the history of his diagnosis, information about the physical examination, your lifestyle, diagnoses and prognoses; results about the treatment and procedures undergone. 
                                It is a process in which the patient information is been electronically saved or stored health information in a digital format that is easily accessible through online.EMR is very handy. </p>
                   
                 
         

     </div>
</div>
    

    <!-- Feature Section -->
        
        
        
        <!-- Featured Work -->
        
        <section id="feature-work" class="protfolio-padding">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="title">
                            <h3>HOW DOES <span>EMR HELPS YOU</span></h3>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="featured-list">
                <div id="grid" class="clearfix">
                    
                    <div class="thumb">
                        <a href="#">
                            <img src="img/protfolio1.jpg" alt="Feature Image" />
                        </a>
                        
                        <div class="thumb-rollover">
                            <div class="project-title">
                                <h4>Patient records identification and maintenance</h4>
                                <h5>Improved communication between healthcare settings and health professionals – within hospitals and across the healthcare system</h5>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="thumb">
                        <a href="#">
                            <img src="img/protfolio8.jpg" alt="Feature Image" />
                        </a>
                        <div class="thumb-rollover">
                            <div class="project-title">
                                <h4>Managing and maintaining patient demographics </h4>
                                <h5>EMRs that can transport patient information in real time with the click of a button</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="thumb">
                        <a href="#">
                            <img src="img/protfolio4.jpg" alt="Feature Image" />
                        </a>
                        
                        <div class="thumb-rollover">
                            <div class="project-title">
                                <h4>To lower cost and improve quality</h4>
                                <h5>Prevent folders holding countless pieces of patient information and scribbled notes</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="thumb">
                        <a href="#">
                            <img src="img/protfolio8.jpg" alt="Feature Image" />
                        </a>
                        
                        <div class="thumb-rollover">
                            <div class="project-title">
                                <h4>Access the largest network of labs and imaging centers</h4>
                                <h5>Alerts about allergies and other important health-related issues</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="thumb">
                        <a href="#">
                            <img src="img/protfolio5.jpg" alt="Feature Image" />
                        </a>
                        
                        <div class="thumb-rollover">
                            <div class="project-title">
                                <h4>EMRs minimize waste and inefficiency of manual and paper-based processes</h4>
                                <h5>Easily identify the details of all the patient treated by doctor</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="thumb">
                        <a href="#">
                            <img src="img/protfolio7.jpg" alt="Feature Image" />
                        </a>
                        
                        <div class="thumb-rollover">
                            <div class="project-title">
                                <h4>EMRs health information is accessible to providers of health care and to the patients </h4>
                                <h5>Check how their patients are doing on certain parameters—such as blood pressure readings or vaccinations</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="thumb">
                        <a href="#">
                            <img src="img/protfolio3.jpg" alt="Feature Image" />
                        </a>
                        
                        <div class="thumb-rollover">
                            <div class="project-title">
                                <h4>Application supported in all devices </h4>
                                <h5>Best practice decision making</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="thumb">
                        <a href="#">
                            <img src="img/protfolio8.jpg" alt="Feature Image" />
                        </a>
                        
                        <div class="thumb-rollover">
                            <div class="project-title">
                                <h4>Guidance for clinical assessments</h4>
                                <h5>Document regarding the patient can be stored/retrieved at any time anywhere</h5>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            
            
        </section>
        
    <!-- Featured Work -->
    <!-- Header End -->
       
        
        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.mousewheel-3.0.6.pack.js"></script>
        <script src="js/paralax.js"></script>
        <script src="js/jquery.smooth-scroll.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/main.js"></script>
        
        
        <script type="text/javascript">
            $(document).ready(function(){
                $('a[href^="#"]').on('click',function (e) {
                    e.preventDefault();

                    var target = this.hash;
                    var $target = $(target);

                    $('html, body').stop().animate({
                         'scrollTop': $target.offset().top
                    }, 900, 'swing');
                    });
            });
        </script>
        
        <script src="js/custom.js"></script>
        

         <!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var $_Tawk_API={},$_Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/58201f1ae0eacb0f20e3ffcd/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->

        <script type="text/javascript" src="//s3.amazonaws.com/downloads.mailchimp.com/js/signup-forms/popup/embed.js" data-dojo-config="usePlainJson: true, isDebug: false"></script><script type="text/javascript">require(["mojo/signup-forms/Loader"], function(L) { L.start({"baseUrl":"mc.us13.list-manage.com","uuid":"2536ce67358a0b9087152d342","lid":"05e9e0f66e"}) })</script>
        
<br>
<br>
<br>
<br>
<br>
<br>
<!-- footer -->
<?php 
include'footer.php';
?>
<!-- footer -->

        </body>
</html>
