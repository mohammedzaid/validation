<?php 

session_start();
	require 'database-config.php';
        
            $link = mysql_connect($host, $user, $pass);
    if(!$link) {
        die('Failed to connect to server: ' . mysql_error());
    }
    
    //Select database
    $db = mysql_select_db($database);
    if(!$db) {
        die("Unable to select database");

    }
 
	require 'database.php';

	$id = null;
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( null==$id ) {
		header("Location: admin_home.php");
	}
	
	if ( !empty($_POST)) {

		// keep track validation errors
		$nameError = null;
		$addressError = null;
		$phoneError = null;
		$mail_idError = null;
		$HospitalError = null;
		$DieaseError = null;
		$roleError = null;

		
		// keep track post values
		$name = $_POST['name'];
		$address = $_POST['address'];
		$phone = $_POST['phone'];
		$mail_id = $_POST['mail_id'];
		$Hospital = $_POST['Hospital'];
		$Diease = $_POST['Diease'];
		$role = $_POST['role'];
		
		
		
		
		// validate input
		$valid = true;
		if (empty($name)) {
			$nameError = 'Please enter Name';
			$valid = false;
		}
		
		if (empty($address)) {
			$addressError = 'Please enter address Address';
			$valid = false;
		} 

		if (empty($phone)) {
			$phoneError = 'Please enter Mobile Number';
			$valid = false;
		}
		
		if (empty($mail_id)) {
			$mail_idError = 'Please enter Mail Id';
			$valid = false;
		}

		if (empty($Hospital)) {
			$HospitalError = 'Please enter Hospital Name';
			$valid = false;
		}

		if (empty($Diease)) {
			$DieaseError = 'Please enter Disease Handled';
			$valid = false;
		}

		if (empty($role)) {
			$roleError = 'Please enter role';
			$valid = false;
		}

		


		
		// update data
		if ($valid) {
			if ($_SESSION['sess_userrole']=='admin') {
			$pdo = Database::connect();
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$sql = "UPDATE login  set name = ?, address = ?, phone = ?,mail_id =?, Hospital =?, Diease =?, role =?, WHERE id = ?";
			$q = $pdo->prepare($sql);
			$q->execute(array($name,$address,$phone,$mail_id,$Hospital,$Diease,$role,$id));
			Database::disconnect();
			echo "<SCRIPT LANGUAGE='JavaScript'> window.alert('Successfully Updated'); window.location.href='admin_home.php'; </SCRIPT>";
			
			}
		else
{
	echo "<SCRIPT LANGUAGE='JavaScript'> window.alert('Sign in again'); window.location.href='indexlog.php'; </SCRIPT>";
			Database::disconnect();

}
		}
	} else {
		if ($_SESSION['sess_userrole']=='admin') {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM login where id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$name = $data['name'];
		$address = $data['address'];
		$phone = $data['phone'];
		$mail_id = $data['mail_id'];
		$Hospital = $data['Hospital'];
		$Diease = $data['Diease'];
		$role = $data['role'];
		
		
		Database::disconnect();
		}
		else
{
	echo "<SCRIPT LANGUAGE='JavaScript'> window.alert('Sign in again'); window.location.href='indexlog.php'; </SCRIPT>";
			Database::disconnect();

}
	}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <!-- Font -->
        
        
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>



</head>

<body>
<!-- Navigation -->
<div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a href="index.html" class="navbar-brand"><img src="img/logo.png" alt="Logo" /></a>                          
                            </div>
                            
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="admin_home.php">Admin Home</a></li>
                                    <li><a href="patient.php">Patient Home</a></li>
                                    <li><a href="indexlog.php">Logout</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
 

<!-- Navigation ends -->

<br>
<br>
    <div class="container">
     
      <div class="col-md-6 col-md-offset-3">

                  
                            <div class="block-margin-top">
    		
	    			<form class="form-horizontal" action="update_user.php?id=<?php echo $id?>" method="post">
	    			<table>

	    			<h4></span>Update a Patient Detail    <span class="glyphicon glyphicon-user"></h4>
                  <br/>
	<tr>
	<td style="padding:0 55px 0 15px;">		
					  <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
					    <label class="control-label">Name</label>
					    <div class="controls">
					      	<input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
					      	<?php if (!empty($nameError)): ?>
					      		<span class="help-inline"><?php echo $nameError;?></span>
					      	<?php endif; ?>
					    </div>
					  </div>
	</td>
	<td style="padding:0 55px 0 15px;">				  

						  <div class="control-group <?php echo !empty($phoneError)?'error':'';?>">
					    <label class="control-label">Phone Number</label>
					    <div class="controls">
					      	<input name="phone" type="text" placeholder="PhoneNumber" value="<?php echo !empty($phone)?$phone:'';?>"  onblur="checkLength(this)" maxlength="10" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
					      	<?php if (!empty($phoneError)): ?>
					      		<span class="help-inline"><?php echo $phoneError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>

</td>
</tr>

<tr>
<td colspan="2" style="padding:0 55px 0 15px;">


					  <div class="control-group <?php echo !empty($addressError)?'error':'';?>">
					    <label class="control-label">Address</label>
					    <div class="controls">
					      	<input style="width: 100%;" name="address" type="text" placeholder="Address" value="<?php echo !empty($address)?$address:'';?>"></input>
					      	<?php if (!empty($addressError)): ?>
					      		<span class="help-inline"><?php echo $addressError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>


</td>
</tr>


<tr>
<td style="padding:0 55px 0 15px;">		
					  <div class="control-group <?php echo !empty($mail_idError)?'error':'';?>">
					    <label class="control-label">Email ID</label>
					    <div class="controls">
					      	<input name="mail_id" type="text"  placeholder="Email ID" value="<?php echo !empty($mail_id)?$mail_id:'';?>">
					      	<?php if (!empty($mail_idError)): ?>
					      		<span class="help-inline"><?php echo $mail_idError;?></span>
					      	<?php endif; ?>
					    </div>
					  </div>



					  <div class="control-group <?php echo !empty($HospitalError)?'error':'';?>">
					    <label class="control-label">Name of Hospital</label>
					    <div class="controls">
					      	<input name="Hospital" type="text"  placeholder="Name of Hospital" value="<?php echo !empty($Hospital)?$Hospital:'';?>">
					      	<?php if (!empty($HospitalError)): ?>
					      		<span class="help-inline"><?php echo $HospitalError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
</td>
<td style="padding:0 55px 0 15px;">
					  <div class="control-group <?php echo !empty($DieaseError)?'error':'';?>">
					    <label class="control-label">Disease Handled</label>
					    <div class="controls">
					      	<input name="Diease" type="text"  placeholder="Disease Handled" value="<?php echo !empty($Diease)?$Diease:'';?>">
					      	<?php if (!empty($DieaseError)): ?>
					      		<span class="help-inline"><?php echo $DieaseError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>



					  <div class="control-group <?php echo !empty($roleError)?'error':'';?>">
					    <label class="control-label">Role</label>
					    <div class="controls">
					      	<input name="role" type="text"  placeholder="Role" value="<?php echo !empty($role)?$role:'';?>">
					      	<?php if (!empty($roleError)): ?>
					      		<span class="help-inline"><?php echo $roleError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
</td>
</tr>

<tr>
<td colspan="2" style="padding:0 55px 0 15px;">

<br>
					  <div class="form-actions">
						  <button style="width: 210px;" type="submit" class="btn btn-success">Update</button>
						  <a style="width: 210px;" class="btn btn-success" href="admin_home.php">Back</a>
						</div>


</td>
</tr>
						

</table>

					
					 
					
					</form>

			
                           </div>
                    </div>

               </div>
            
           </div> <!-- /container -->
<br>
<br>
<br>
<!-- footer -->
<?php 
include'footer.php';
?>
<!-- footer -->

 <!-- popups messagebox for phone validation -->
<script type="text/javascript">
  function checkLength(el) {
  if (el.value.length != 10) {
    alert("Invalid Number.. please enter 10 digit number")
  }
}
</script>
<!-- phone valiation -->


</body>
</html>
