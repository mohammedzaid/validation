<?php

session_start();
	     $name=$_SESSION['name'];
	     $id=$_SESSION['id'];
             $address=$_SESSION['address'];
             $refby=$_SESSION['refby'];
             $mobile=$_SESSION['mobile'];
             $dob=$_SESSION['dob'];
             $date_report=$_SESSION['date_report'];
	     	 $age=$_SESSION['age'];
             $height=$_SESSION['height'];
             $weight=$_SESSION['weight'];
             $gender=$_SESSION['gender'];
             $diagnosis=$_SESSION['diagnosis'];
             $HgA1c_Date=$_SESSION['HgA1c_Date'];
             $HgA1c=$_SESSION['HgA1c'];
             $cholesterol_date=$_SESSION['cholesterol_date'];
             $LDL_c=$_SESSION['LDL_c'];
             $HDL=$_SESSION['HDL'];
             $Trig= $_SESSION['Trig'];
             $comments=$_SESSION['comments'];
             $interpretation=$_SESSION['interpretation'];
             $test=$_SESSION['test'];
             $Hospital=$_SESSION['Hospital'];


//============================================================+
// File name   : diabetesrep.php
// Last Update : 2016-10-21
// Author      : CK
// (c) Copyright:CK
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author CK
 * @since 2016-10-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
class MYPDF extends TCPDF {


	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);

		// Page number
		$this->Cell(0, 10, 'EMR MED SERVICES', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('CK');
$pdf->SetTitle('DIABETES REPORT');
$pdf->SetSubject('REPORT GENERATION');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$image=$_SESSION['logo'];

// set default header data
$pdf->SetHeaderData("$image", PDF_HEADER_LOGO_WIDTH, "$Hospital", "Laboratory Testing in $test
 Examined By : $refby");

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 10);

// add a page
$pdf->AddPage();


// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content



// output some RTL HTML content
$html  = '<h3 style="color:#2cb8b8;" align="center">Laboratory Investigation </h3><br>
<table border="0" cellspacing="2" cellpadding="2">
    <tr>
		<th align="left"><b>Patient Name: </b></th>
		<td align="left">'.$name.'</td>
		<th align="left"><b>Age:</b></th>
		<td align="left">'.$age.'</td>
	</tr>
	<tr>
		<th style="left"><b>Address: </b></th>
		<td align="left">'.$address.'</td>
		<th align="left"><b>Date of Report: </b></th>
		<td align="left">'.$date_report.'</td>
	</tr>
	<tr>
		<th align="left"><b>Referring Physician: </b></th>
		<td align="left">'.$refby.'</td>
		<th align="left"><b>Type of Scan: </b></th>
		<td align="left">'.$test.'</td>
	</tr>
	</tr>
</table>
    <br> 
    <h3 style="color:#2cb8b8;" align="center">Diabetes Function Tests</h3>
    <h4 style="color:#2cb8b8;" align="center">Detailed Report</h4><br>
	<table border="0" cellspacing="5" >
	<tr>
		<th align="left"><b>Test</b></th>
		<th align="left"><b>Values</b></th>
		<th align="left"><b>Readings</b></th>
	</tr>
	
	<tr>
		<th align="left">LDL-c </th>
		<th align="left">'.$LDL_c.'</th>
		<th align="left">Normal</th>
	</tr>

	<tr>
		<th align="left">HDL </th>
		<th align="left">'.$HDL.'</th>
		<th align="left">Normal</th>
	</tr>

	<tr>
		<th align="left">Trig</th>
		<th align="left">'.$Trig.'</th>
		<th align="left">Normal</th>
	</tr>

	<tr>
		<th align="left">HgA1c</th>
		<th align="left">'.$HgA1c.'</th>
		<th align="left">Normal</th>
	</tr> 

	<h3 style="color:#2cb8b8;" align="left">Comments : </h3> '.$comments.' <br><br>

	<h3 style="color:#2cb8b8;" align="left">Interpretation :</h3> '.$interpretation.'  <br><br><br>

	<img src="images/sig1.png" alt="test alt attribute" width="90" height="40" border="0" /><br><br>

	<h4  align="left">Interpreted & dictated by Dr Robert Peiss</h4><br><br>
	<h5  align="left">Consultant Endocrinology</h5><br><br>
	</table>
	<br>

';

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table
// add a page
$pdf->AddPage();

// create some HTML content
$html = '<h3 style="color:#2cb8b8;" align="left">PROCEDURE :</h3><br>
<p>This is a gated SPECT Myoview stress/rest myocardial perfusion scan, with the resting
images obtained after the intravenous injection of 16.4 mCi of Technetium-99m Myoview
injected into a right wrist vein. The post-stress images were obtained after the intravenous
injection of 33.3 mCi of Technetium-99m Myoview injected into a right wrist vein.</p>

<h3 style="color:#2cb8b8;" align="left">INDICATION :</h3><br>
Hypertension, diabetes mellitus, palpitations, fatigue, and tiredness.

<h3 style="color:#2cb8b8;" align="left">FINDINGS : :</h3><br>
<p>The resting heart rate is 79 beats/minute, the resting blood pressure is 162/82, the peak heart
rate is 110 beats/minute, and the peak blood pressure is 180/80. The patient achieved 75%
of the target heart rate and the study was terminated due to shortness of breath and fatigue.
The patient had no chest pain on the treadmill. The stress EKG is normal, with no significant
ST segment changes or cardiac arrhythmias.
The left ventricular ejection fraction is 72%, and the end-diastolic volume 106 mL, both of
which are normal. The wall motion is normal. There is a large, moderate to severe,
reversible myocardial perfusion defect involving the septum, apex, and inferior wall of the left
ventricle. The quantitative analyses confirm this finding.</p>

<h3 style="color:#2cb8b8;" align="left">IMPRESSION : </h3><br>

Abnormal gated SPECT Myoview stress/rest myocardial perfusion scan.
1. Normal left ventricular ejection fraction, normal left ventricular cavity size, and normal wall
motion.
2. Large, moderate to severe, reversible myocardial perfusion defect involving the septum,
apex, and inferior wall of the left ventricle.
3. The stress EKG supervising and interpreting cardiologist is Dr. Kathleen Ward. The stress
EKG findings are reported under separate cover.

<br><br>
<img src="images/sig2.png" alt="test alt attribute" width="100" height="60" border="0" />
	<h4  align="left">Interpreted & dictated by Dr John Peter</h4><br><br>
	<h5  align="left">Consultant Orthology</h5><br><br>';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print all HTML colors

// add a page


// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('diabetes_report.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+





