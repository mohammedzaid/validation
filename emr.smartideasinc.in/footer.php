<!-- Copyright -->
   <footer class="site-footer">
    <div class="copyright">
        <div class="container">
          <div class="row">
            <div class="col-sm-4">
              <div class="copy-text">
                <p>Copyright 2016 © <strong><a href="http://emr.smartideasinc.in/">EMR Med Services</a></strong><br>
                Developed By <strong><a href="http://smartideasinc.in/">SmartIdeas Inc</a></strong></p>
              </div>
            </div>
            <div class="col-sm-5">
              <div class="footer-menu pull-right">
                <ul>
                  <li><a href="aboutus.php">EMR</a></li>
                  <li><a href="#features">Terms</a></li>
                  <li><a href="#faq">Privacy Policy</a></li>
		              <li><a href="demo_signup.php">Sign up</a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="social">
                <ul>
                  <li><a href="https://www.facebook.com/smartideasinc/"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="https://twitter.com/smartideas013"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="https://www.linkedin.com/company/smart-ideas-inc"><i class="fa fa-linkedin"></i></a></li>
                  <li><a href="info@smartideasinc.in"><i class="fa fa-google-plus"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
     </footer>
<!-- footer -->
