<?php 
	session_start();
	require 'database.php';
	$id = null;
	if ( !empty($_GET['id'])) {
		$id = $_REQUEST['id'];
	}
	
	if ( null==$id ) {
		header("Location: admin_home.php");
	}
 elseif ($_SESSION['sess_userrole']=='admin') {
		$pdo = Database::connect();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM login where id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		Database::disconnect();
	}
else
{
	echo "<SCRIPT LANGUAGE='JavaScript'> window.alert('Sign in again'); window.location.href='indexlog.php'; </SCRIPT>";
			Database::disconnect();

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>EMR MED REPORT</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
         <!-- Font -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
<!-- Navigation -->
<div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle Navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                
                                <a href="index.html" class="navbar-brand"><img src="img/logo.png" alt="Logo" /></a>                          
                            </div>
                            
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="index.html">Home</a></li>
                                    <li><a href="admin_home.php">Admin Home</a></li>
                                    <li><a href="patient.php">Patient Home</a></li>
                                    <li><a href="indexlog.php">Logout</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
 

<!-- Navigation ends -->
<br>
<br>
    <div class="container">
    
    			<div>
    				<div class="row">
		    			<h3>Read a Patient Detail</h3>
		    		</div>
		    		
	    			<div class="form-horizontal" >
	    			<table style="width:100%">
					  <tr>
					    <th>Field</th>
					    <th>Value</th>
						<th>Field</th>
					    <th>Value</th>
					</tr>
					  <tr>
					    <th>Name :</th>
					    <td><?php echo $data['name'];?></td>
					    <th>Address :</th>
					    <td><?php echo $data['address'];?></td>
					  </tr>
					  <tr>
					    <th>Phone :</th>
					    <td><?php echo $data['phone'];?></td>
					    <th>Mail id</th>
					    <td><?php echo $data['mail_id'];?></td>
					  </tr>
					    <tr>
					    <th>Hospital Name</th>
					    <td><?php echo $data['Hospital'];?></td>
					    <th>Disease Handled</th>
					    <td><?php echo $data['Diease'];?></td>
					  </tr>
					    
					    
					</table>
         <div class="form-actions">
		<a class="btn btn-success" href="admin_home.php">Back</a>
	</div>
</div>
</div>
				
    </div> <!-- /container -->

<!-- footer -->
<?php 
include'footer.php';
?>
<!-- footer -->

  </body>
</html>
